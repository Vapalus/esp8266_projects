#ifndef WIFISCANNER
#define WIFISCANNER

#include "ESP8266WiFi.h"
#include "Pinger.h"
#include "KeyButtons.h"
extern "C"
{
  #include <lwip/icmp.h> // needed for icmp packet definitions
}

#define WIFISCANNER_RSSI_LEN 5
#define WIFISCANNER_MAX_COUNT 15
#define WIFISCANNER_MAX_SHOWN 5
#define WIFISCANNER_SSID_LENGTH 24


class WifiScanner{
  private:
    static int CHANs[WIFISCANNER_MAX_COUNT];// = {0};
    static char SSIDs[WIFISCANNER_MAX_COUNT][WIFISCANNER_SSID_LENGTH];// = {{0}};
    static volatile bool blnNetworksShown;
    static int iNetworkCount;
    static void networksFound(int networksFound){
      iNetworkCount = networksFound;
      int iStart = 0;
      int iStepSize = 12;
      if (networksFound == 0) {
        iNetworkCount = 0;
      } else {
        int iOpenWifiHotspots = 0;
        for (int i = 0; i < networksFound && i < WIFISCANNER_MAX_COUNT; ++i) {
          String str = WiFi.SSID(i);
          int length = str.length() < WIFISCANNER_SSID_LENGTH ? str.length() : WIFISCANNER_SSID_LENGTH;
          str.toCharArray(SSIDs[i], WIFISCANNER_SSID_LENGTH);
          int j;
          while(SSIDs[i][++j] && j < WIFISCANNER_SSID_LENGTH);
          
          if (WiFi.encryptionType(i) != ENC_TYPE_NONE){
            if (length + 1 >= WIFISCANNER_SSID_LENGTH){
              SSIDs[i][length-2] = '*';
              SSIDs[i][length-1] = 0;
            }
            else{
              SSIDs[i][length] = '*';
              SSIDs[i][length+1] = 0;
            }
          }
          
          CHANs[i] = WiFi.RSSI(i);
          iOpenWifiHotspots++;
        }
        blnNetworksShown = true;
      }
    }
    
    static void showNetworks(SSD1306Wire* display, KeyButtons* keyboard, int networksFound){
      static int iShowNext = 0;
      //static int iWaitTime = WIFISCANNER_LISTSWITCH_TIME;
      
      int i = 0;
      int iStart = 0;
      int iStepSize = 12;
      if (iShowNext >= WIFISCANNER_MAX_COUNT || iShowNext >= iNetworkCount){
        iShowNext = 0;
      }
      for(i = iShowNext; i < WIFISCANNER_MAX_COUNT && (i - iShowNext) < WIFISCANNER_MAX_SHOWN && i < iNetworkCount; i++){
        if (!SSIDs[i][0]){
          display->drawString(0,iStart + iStepSize*(i - iShowNext), "-");
        }
        char toShow[2] = {i+'0',0};
        display->drawString(0,iStart + iStepSize*(i - iShowNext), toShow);
        display->drawString(13,iStart + iStepSize*(i - iShowNext), SSIDs[i]);
        char cstr[WIFISCANNER_RSSI_LEN];
        itoa(CHANs[i],cstr,10);
        cstr[WIFISCANNER_RSSI_LEN-1] = 0;
        if (CHANs[i] < 10){
          display->drawString(110,iStart + iStepSize*(i - iShowNext), cstr);
        }
        else{
          display->drawString(104,iStart + iStepSize*(i - iShowNext), cstr);
        }
      }
      if (keyboard->wasPressed(5)){
        //iWaitTime = WIFISCANNER_LISTSWITCH_TIME;
        iShowNext = i;
      }
    }
  public:
  
    static void init(KeyButtons* keyboard){
      WiFi.mode(WIFI_STA);
      WiFi.disconnect();
      blnNetworksShown = true;
      iNetworkCount = 0;
      keyboard->activateKey(5);
    }

    static void deinit(){
      iNetworkCount = 0;
      /*
      for (int i = 0; i < 20 && WiFi.scanComplete() == -1; i++){
        delay(100);
      }
      */
      WiFi.scanDelete();
    }
    
    static void getScan(SSD1306Wire* display, KeyButtons* keyboard){
      if (blnNetworksShown){
        blnNetworksShown = false;
        WiFi.scanNetworksAsync(networksFound, true);
      }
      if (iNetworkCount){
        showNetworks(display, keyboard, iNetworkCount);
      }
      else{
        display->drawString(0,0, "waiting for scan");
      }
    }
};
int WifiScanner::CHANs[WIFISCANNER_MAX_COUNT] = {0};
char WifiScanner::SSIDs[WIFISCANNER_MAX_COUNT][WIFISCANNER_SSID_LENGTH] = {{0}};
volatile bool WifiScanner::blnNetworksShown = false;
int WifiScanner::iNetworkCount = 0;
#endif
