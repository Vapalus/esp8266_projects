#ifndef INTERNETFUNCTIONS
#define INTERNETFUNCTIONS

#include <ESP8266WiFi.h>
#include "SSD1306Wire.h"
#include "KeyButtons.h"

//definitely NOT good programming style; this belongs at the place the data is at.
#define INTERNETFUNCTIONS_MUSIC_COUNT 32
#define INTERNETFUNCTIONS_GAMES_COUNT 10
#define INTERNETFUNCTIONS_TIMEOUT_TIME 250

//This class could be better: All the functions could be declared in the class body, and implemented later on. It is ugly the way I did it, and it's because of pure lazyness.
class InternetFunctions{
  private:
    const static char* game[];
    const static char* mednafen_name[];
    const static char* music[];
    const static char* link[];
    const static char* searchphrase[];

    static String _strIP;
    static String _strURL;



    static bool sendPacket(String strIP, String strURL){
      WiFiClient client;
      client.setTimeout(INTERNETFUNCTIONS_TIMEOUT_TIME);
      if (!client.connect(strIP, 80)) {
        client.stop();
        _strIP = strIP;
        _strURL = strURL;
        return true;
      }

      client.print(String("GET ") + strURL + " HTTP/1.1\r\n" + "Host: " + strIP + "\r\n" + "Connection: close\r\n\r\n");
      
      for (int i = 0; i < 20 && (client.connected() || client.available()); i++)
      {
        while (client.available())
        {
          client.read();
        }
        delay(10);
      }
      _strIP = "";
      _strURL = "";
      client.stop();
      return true;
    }
    
    static bool repeatPacket(){
        if (_strIP.length() == 0 && _strURL.length() == 0){
          sendPacket(_strIP, _strURL);
        }
    }
    
    static bool sendDeviceData(uint8_t device, uint8_t data0, uint8_t data1, uint8_t data2){
      String strData0 = String(data0, DEC); 
      String strData1 = String(data1, DEC); 
      String strData2 = String(data2, DEC); 
      String dataSet = strData0+":"+strData1+":"+strData2+":";
      String strDevice = String(device, DEC);
      String ip = "192.168.178.56";
      String url = "/?ambient=" + strDevice + "&colors=" + dataSet;
      return sendPacket(ip, url);
    }

    static bool controlDevices(SSD1306Wire* display, KeyButtons* keyboard){
      static uint8_t device = 2;
      static uint8_t data0 = 255;
      static uint8_t data1 = 128;
      static uint8_t data2 = 0;

      bool result = true;

      repeatPacket();
      
      if (keyboard->wasPressed(10)){
        device++;
      }
      if (keyboard->wasPressed(15)){
        device--;
      }
      if (keyboard->wasPressed(8)){
        data0 = 0;
        data1 = 0;
        data2 = 0;
        result = sendDeviceData(device,data0,data1,data2);
      }
      if (keyboard->wasPressed(9)){
        data0 = 255;
        data1 = 100;
        data2 = 0;
        result = sendDeviceData(device,data0,data1,data2);
      }

      //Just testing out the easiest way, don't judge me.
      char deviceString[] = {'0'+device,0};
      char data0String[] = {'0'+data0/100,'0'+(data0/10)%10,'0'+data0%10,0};
      char data1String[] = {'0'+data1/100,'0'+(data1/10)%10,'0'+data1%10,0};
      char data2String[] = {'0'+data2/100,'0'+(data2/10)%10,'0'+data2%10,0};
      
      display->drawString(5,5, deviceString);
      display->drawString(5,15, data0String);
      display->drawString(25,15, data1String);
      display->drawString(50,15, data2String);

      return result;
    }
  
    static bool sendMusicRequest(String strSongName, bool blnStop, bool blnDirectRequest){
      String url;
      if (blnStop){
        url = "/cgi-bin/queryString.sh?youtube-off";
      }
      else{
        if (blnDirectRequest){
          url = "/cgi-bin/queryString.sh?youtube-background-novideo-nosearch-" + strSongName;
        }
        else{
          strSongName.replace(' ','+');
          url = "/cgi-bin/queryString.sh?youtube-one-novideo-one-" + strSongName;
        }
      }
      String ip = "192.168.178.35";
      
      return sendPacket(ip, url);
    }
        
    static bool controlMusic(SSD1306Wire* display,KeyButtons* keyboard){
      static int iCurrentMusic = 0;
      display->drawString(5,5, "Music");
      
      if (keyboard->wasPressed(10)){
        iCurrentMusic++;
      }
      if (keyboard->wasPressed(15)){
        iCurrentMusic--;
      }
      while (iCurrentMusic < 0){
        iCurrentMusic += INTERNETFUNCTIONS_MUSIC_COUNT;
      }
      iCurrentMusic %= INTERNETFUNCTIONS_MUSIC_COUNT;

      display->drawString(5,25, music[iCurrentMusic]);
      if (keyboard->wasPressed(8)){
        display->drawString(5,35, "sending...");
        //sendMusicRequest(String(link[iCurrentMusic]),!iCurrentMusic, true); //would work, too
        return sendMusicRequest(String(link[iCurrentMusic]), iCurrentMusic == 0, true);
      }
      return true;
    }

    static bool sendGameRequest(String strGameName, bool blnStop){
      String url;
      if (blnStop){
        url = "/cgi-bin/queryString.sh?youtube-off"; //Yes I know this is irritating, but that's how my stuff rolls, motherfucker.
      }
      else{
        url = "http://tv/cgi-bin/queryString.sh?mednafen-play-" + strGameName;
      }
      String ip = "192.168.178.35";
      
      return sendPacket(ip, url);
    }
    
    static bool controlGames(SSD1306Wire* display,KeyButtons* keyboard){
      static int iCurrentGame = 0;
      display->drawString(5,5, "Games");
      
      if (keyboard->wasPressed(10)){
        iCurrentGame++;
      }
      if (keyboard->wasPressed(15)){
        iCurrentGame--;
      }
      while (iCurrentGame < 0){
        iCurrentGame += INTERNETFUNCTIONS_GAMES_COUNT;
      }
      iCurrentGame %= INTERNETFUNCTIONS_GAMES_COUNT;

      display->drawString(5,25, game[iCurrentGame]);
      if (keyboard->wasPressed(8)){
        display->drawString(5,35, "sending...");
        //sendMusicRequest(String(link[iCurrentMusic]),!iCurrentMusic, true); //would work, too
        return sendGameRequest(String(mednafen_name[iCurrentGame]), iCurrentGame == 0);
      }
      return true;
    }
    
    static bool setVolume(SSD1306Wire* display,KeyButtons* keyboard){
      static int iCurrentVolume = 0;
      display->drawString(5,5, "Volume");
      char volume[] = "____________________";
      volume[iCurrentVolume+1] = 0;
      //epic lazyness
      display->drawString(0,7, volume);
      display->drawString(0,8, volume);
      
      if (keyboard->wasPressed(10) || keyboard->wasPressed(9)){
        if (iCurrentVolume < 21){
          iCurrentVolume++;
          String ip = "192.168.178.35";
          String url = "/cgi-bin/queryString.sh?volume-" + String(iCurrentVolume, DEC);
          display->drawString(5,35, "sending...");
          return sendPacket(ip, url);
        }
      }
      if (keyboard->wasPressed(15) || keyboard->wasPressed(8)){
        if (iCurrentVolume > -1){
          iCurrentVolume--;
          String ip = "192.168.178.35";
          String url = "/cgi-bin/queryString.sh?volume-" + String(iCurrentVolume, DEC);
          display->drawString(5,35, "sending...");
          return sendPacket(ip, url);
        }
      }
      return true;
    }
  
  public:
  
    static void init(KeyButtons* keyboard){
      keyboard->activateKey(5);
      keyboard->activateKey(6);
      keyboard->activateKey(7);
      keyboard->activateKey(8);
      keyboard->activateKey(10);
      keyboard->activateKey(15);
    }
    
    static bool doInternetStuff(SSD1306Wire* display,KeyButtons* keyboard){
      static int currentState = 0;
      if (keyboard->wasPressed(5)){
        currentState += 1;
      }
      if (keyboard->wasPressed(6)){
        currentState -= 1;
      }
      bool result = true;
      switch(currentState){
        case 0:
          result = controlDevices(display,keyboard);
          break;
        case 1:
          result = controlMusic(display,keyboard);
          break;
        case 2:
          result = setVolume(display,keyboard);
          break;
        case 3:
          result = controlGames(display,keyboard);
          break;
        default:
          result = controlDevices(display,keyboard);
          if (currentState < 0){
            currentState = 3; //maybe solve this one
          }
          else{
            currentState = 0;
          }
          break;
      }

      return result;
    }
};

String InternetFunctions::_strIP = "";
String InternetFunctions::_strURL = "";

//In my honest opinion, this is the worst way of doing it.
//Reason is: If you do it like that and add music, the reference between "music" and "link" (which belong together) might be lost. This is a bad example. Especially if it is, in this case, links.
//You need to look twice to check if "Something Wicked" and "Zuw_O5MU5CE" belong together - or not.
//I still did it that way to save memory, but I'm thinking about a better way right now. Probably object oriented.
const char* InternetFunctions::game[] = {
  "Off",
  "Pokemon",
  "Mario Land",
  "Wario Land",
  "Metal Gear Solid",
  "Duke Nukem",
  "Kirby",
  "Doom I",
  "Doom II",
  "Legend of Zelda",
};
const char* InternetFunctions::mednafen_name[] = {
  "",
  "pokemon",
  "marioland",
  "wario",
  "metalgear",
  "duke",
  "kirby",
  "doom",
  "doom2",
  "zelda",
};

const char* InternetFunctions::music[] = {
  "Off",
  "Something Wicked",
  "Artemisias Childhood",
  "Cao Cao: A short song",
  "Song Wa: flute tunes",
  "Hita: Love is hard to fall",
  "Waltz with Vampires",
  "Bow",
  "Blood Hunter",
  "Demise of a Nation",
  "Raknarok",
  "Blood Red Roses",
  "Otar the Foul",
  "Lucifers Lullaby",
  "Ruthless Queen",
  "300 - Marathon",
  "Satan's Arrival",
  "Last Yaeger",
  "Power of Will",
  "Overpowered",
  "Forefathers Eve",
  "Mirror Master (Witcher)",
  "Doom OST",
  "Prodigy Spitfire",
  "Clint Eastwood",
  "Forget about Freeman",
  "Mechanicus",
  "Rise of the black curtain",
  "Mangelan Matrix",
  "Where is your god now",
  "Jazz Music",
  "Jazz/Bossa Nova",
};
const char* InternetFunctions::link[] = {
  "",
  "Zuw_O5MU5CE",
  "PuNrMajsDzA",
  "cC2of_7cC-o",
  "KiGuO5r1le0",
  "4ROBQMlh3Ew",
  "dl30WDn_P2o",
  "kkcq3tX2nOA",
  "Y0uwl6ExlFQ",
  "eduwBgDcMwY",
  "a6WpnHM2kJw",
  "hosCuzo6JKo",
  "SkzriS8aDfc",
  "ipCIlaij39E",
  "vPNvZHmMD6w",
  "eomNCY5fpWY",
  "_8BwHHEopec",
  "fVi27gIbItQ",
  "gh3VJ1E36W4",
  "nvRDAPE1Eac",
  "vOlPwt5ZtB8",
  "kKGmZN06lBI",
  "Jm932Sqwf5E",
  "aQZDbBGBJsM",
  "1V_xRb0x9aw",
  "9OacXUzTNXo",
  "ztzq05IzYds",
  "bA631oqahPA",
  "O8_FLPj6Exo",
  "bsvzP8EO65w",
  "neV3EPgvZ3g",
  "la-wyQI_fFw", 
};
//Not implemented yet
const char* InternetFunctions::searchphrase[] = {

};

#endif
