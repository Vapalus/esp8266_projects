#ifndef FRIGGING_TETRIS
#define FRIGGING_TETRIS

#include "SSD1306Wire.h"
#include "KeyButtons.h"


#define TETRIS_FIELD_WIDTH 32
#define TETRIS_FIELD_HEIGHT 32
#define TETRIS_MAX_PIECETYPES 7
#define TETRIS_BLOCK_SIZE 2

uint8_t iTestCounter = 10;

class TetrisPiece{
  private:
    static uint8_t _rotation;
    static uint8_t _xPos;
    static uint8_t _yPos;
    //The "const" just goes for the "uint8_t" values inside the array, not for the pointers pointing at that array.
    const static uint8_t* _element;

    const static uint8_t pieceTypes[];
  public:
    static void init(uint8_t xStart, uint8_t yStart, uint8_t type){
      _xPos = xStart;
      _yPos = yStart;
      //God, I hate to do this. Shooting with pointers at arrays is typical C-type stuff...
      //Basically, I initialized "pieceTypes" before, and now I'm just grabbing pieces of the array and pretend it's a different 4-piece array by assigning the pointer.
      _element = &pieceTypes[type*16];
      _rotation = 0;
    }
    
    static void drawPiece(SSD1306Wire* display, uint8_t xOffset, uint8_t yOffset){
      if (!iTestCounter){
        _rotation++;
        iTestCounter = 10;
      }
      iTestCounter--;
      for (int i = 0; i < 4; i++){
        for (int j = 0; j < 4; j++){
          if (_element[i+j*4]){
            int x = 0;
            int y = 0;
            switch (_rotation){
              case 0:
                x = (_xPos+i)*TETRIS_BLOCK_SIZE;
                y = (_yPos+j)*TETRIS_BLOCK_SIZE;
                x-=TETRIS_BLOCK_SIZE;
                y-=TETRIS_BLOCK_SIZE;
                break;
              case 1:
                x = (_xPos-j)*TETRIS_BLOCK_SIZE;
                y = (_yPos+i)*TETRIS_BLOCK_SIZE;
                x+=TETRIS_BLOCK_SIZE;
                y-=TETRIS_BLOCK_SIZE;
                break;
              case 2:
                x = (_xPos-i)*TETRIS_BLOCK_SIZE;
                y = (_yPos-j)*TETRIS_BLOCK_SIZE;
                x+=TETRIS_BLOCK_SIZE;
                y+=TETRIS_BLOCK_SIZE;
                break;
              case 3:
                x = (_xPos+j)*TETRIS_BLOCK_SIZE;
                y = (_yPos-i)*TETRIS_BLOCK_SIZE;
                x-=TETRIS_BLOCK_SIZE;
                y+=TETRIS_BLOCK_SIZE;
                break;
              default:
                x = (_xPos+i)*TETRIS_BLOCK_SIZE;
                y = (_yPos+j)*TETRIS_BLOCK_SIZE;
                x-=TETRIS_BLOCK_SIZE;
                y-=TETRIS_BLOCK_SIZE;
                _rotation = 0;
                break;              
            }
            display->drawRect(x+xOffset,y+yOffset,TETRIS_BLOCK_SIZE,TETRIS_BLOCK_SIZE);
          }
        }
      }
    }
    
    static void moveDown(uint8_t left, uint8_t right){
      //y goes DOWN
      _yPos++;
      
    }
};
uint8_t TetrisPiece::_rotation = 0;
uint8_t TetrisPiece::_xPos = 0;
uint8_t TetrisPiece::_yPos = 0;

const uint8_t* TetrisPiece::_element = 0;
const uint8_t TetrisPiece::pieceTypes[] = {
  //Quad
  0,0,0,0,
  0,1,1,0,
  0,1,1,0,
  0,0,0,0,
  //L-Piece
  0,1,0,0,
  0,1,0,0,
  0,1,0,0,
  0,1,1,0,
  //J-Piece
  0,1,0,0,
  0,1,0,0,
  0,1,0,0,
  1,1,0,0,
  //T-Piece
  0,0,0,0,
  1,1,1,0,
  0,1,0,0,
  0,0,0,0,
  //S
  0,0,0,0,
  0,0,1,1,
  1,1,0,0,
  0,0,0,0,
  //Z
  0,0,0,0,
  1,1,0,0,
  0,0,1,1,
  0,0,0,0,
  //dumb line
  0,1,0,0,
  0,1,0,0,
  0,1,0,0,
  0,1,0,0,
};

class TetrisField{
  private:
    //ok, ok. Putting everything in a 32*32 BYTE(!) array is definitely a waste, but I don't want to do bitwise operations at the moment.
    //Later it will be put in a 32*32 bit array. Bit. Not Byte.
    static uint8_t fieldArray[];
    static TetrisPiece currentPiece;
    static uint8_t xOffset;
    static uint8_t yOffset;

    uint8_t getFieldAtPos(uint8_t x, uint8_t y){
      return fieldArray[y*TETRIS_FIELD_WIDTH + x];
    }

    void setFieldAtPos(uint8_t x, uint8_t y, uint8_t value){
      fieldArray[y*TETRIS_FIELD_WIDTH + x] = value;
    }

    void putTetrisPieceAtPos(uint8_t x, uint8_t y, TetrisPiece piece){
      
    }

    void deleteLine(uint8_t y){
      //move everything above the line one down
      for (uint8_t line = y; line < TETRIS_FIELD_HEIGHT - 1; line++){
        for (uint8_t pixel = 0; pixel < TETRIS_FIELD_WIDTH; pixel++){
          setFieldAtPos(pixel,line,getFieldAtPos(pixel,line+1));
        }
      }
      for (uint8_t pixel = 0; pixel < TETRIS_FIELD_WIDTH; pixel++){
        setFieldAtPos(pixel,TETRIS_FIELD_HEIGHT - 1,0);
      }
    }
    
  public:

    void init(){
      //init piece
      currentPiece.init(10,10,2);
      //clear field
      for (uint8_t line = 0; line < TETRIS_FIELD_HEIGHT - 1; line++){
        for (uint8_t pixel = 0; pixel < TETRIS_FIELD_WIDTH; pixel++){
          setFieldAtPos(pixel,line,0);
        }
      }
      
      setFieldAtPos(0,0,1);
      setFieldAtPos(0,1,1);
      setFieldAtPos(0,2,1);
      setFieldAtPos(0,3,1);
      setFieldAtPos(0,4,1);
      setFieldAtPos(0,5,1);

      setFieldAtPos(9,0,1);
      setFieldAtPos(9,1,1);
      setFieldAtPos(9,2,1);
      setFieldAtPos(9,3,1);
      setFieldAtPos(9,4,1);
      setFieldAtPos(9,5,1);

      setFieldAtPos(1,0,1);
      setFieldAtPos(2,0,1);
      setFieldAtPos(3,0,1);
      setFieldAtPos(4,0,1);
      setFieldAtPos(5,0,1);
      setFieldAtPos(6,0,1);

      setFieldAtPos(1,9,1);
      setFieldAtPos(2,9,1);
      setFieldAtPos(3,9,1);
      setFieldAtPos(4,9,1);
      setFieldAtPos(5,9,1);
      setFieldAtPos(6,9,1);
    }
    
    void draw(SSD1306Wire* display){
      currentPiece.drawPiece(display, xOffset, yOffset);
      
      for (uint8_t line = 0; line < TETRIS_FIELD_HEIGHT; line++){
        //can be uint8_t, because due to the display the field will never be larger then 254
        //putting an unsigned int at -1 would work, too
        uint8_t lastPosition = 255;
        for (uint8_t pixel = 0; pixel < TETRIS_FIELD_WIDTH; pixel++){
          uint8_t currentPosition = getFieldAtPos(pixel, line);
          
          if ((currentPosition != 0) && (lastPosition == 255)){
            lastPosition = pixel;
          }
          
          else{
            if ((currentPosition == 0) && (lastPosition != 255)){
              //This basically draws lines because that seemed faster than drawing an image the whole time.
              //it also enables us to check if a line is complete, a.k.a. can be deleted.
              for (uint8_t i = 0; i < TETRIS_BLOCK_SIZE; i++){
                uint8_t x1 = lastPosition*TETRIS_BLOCK_SIZE + xOffset;
                uint8_t y = line*TETRIS_BLOCK_SIZE + i + yOffset;
                uint8_t x2 = pixel*TETRIS_BLOCK_SIZE + xOffset;
                
                display->drawHorizontalLine(x1, y, x2-x1);

                if (x2-x1 == TETRIS_FIELD_WIDTH*TETRIS_BLOCK_SIZE){
                  deleteLine(line);
                }
              }
              lastPosition = 255;
            }
          }
        }
      }
    }

    void run(SSD1306Wire* display, KeyButtons* keyboard){
      draw(display);
    }

    
};
uint8_t TetrisField::xOffset = 3;
uint8_t TetrisField::yOffset = 3;
uint8_t TetrisField::fieldArray[TETRIS_FIELD_WIDTH*TETRIS_FIELD_HEIGHT] = {0};
//TetrisPiece* TetrisField::currentPiece;

#endif
