#ifndef I2C_2313_INTERFACE
#define I2C_2313_INTERFACE
#include "I2C_MANAGEMENT_MASTER.h"


#define I2C_2313_INTERFACE_WORD_LENGTH 6
#define I2C_2313_IO_PINS_COUNT 8
#define I2C_2313_PWM_PINS_COUNT 3

class I2C_2313_Interface{
  private:
    static I2c_management connection;
    static uint8_t pinValues;
    static uint8_t pinInputValues;
    static uint8_t pinInInputMode;
    static uint8_t pwmValues;
    static uint8_t pwm[];
    static uint8_t pinMap[];
    static uint8_t pwmMap[];

  public:
    static void init(){
      connection.init();
    }

    static void update_2313(){
      uint8_t address = 0x08;
      uint8_t values[I2C_2313_INTERFACE_WORD_LENGTH] = {pinValues,pinInInputMode,pwmValues,pwm[0],pwm[1],pwm[2]};
      connection.setValues(address,values,I2C_2313_INTERFACE_WORD_LENGTH);
    }
    
    static void update_self(){
      uint8_t address = 0x08;
      uint8_t values[I2C_2313_INTERFACE_WORD_LENGTH] = {0,0,0,0,0,0};
      connection.getValues(address,values,I2C_2313_INTERFACE_WORD_LENGTH);
      pinInputValues = (pinInInputMode&values[0]);
    }
    
    static void setMode(int iPin, uint8_t iMode){
      uint8_t i = 0;
      for (i = 0; i < I2C_2313_IO_PINS_COUNT; i++){
        if (iPin == pinMap[i]){
          if (iMode){ bitSet(pinInInputMode,i); }
          else{ bitClear(pinInInputMode,i); }
          return;
        }
      }
    }
    
    static void setValue(int iPin, uint8_t iValue){
      uint8_t i = 0;
      for (i = 0; i < I2C_2313_IO_PINS_COUNT; i++){
        if (iPin == pinMap[i]){
          if (iValue){ bitSet(pinValues,i); }
          else{ bitClear(pinValues,i); }
          return;
        }
      }
      for (i = 0; i < I2C_2313_PWM_PINS_COUNT; i++){
        if (iPin == pwmMap[i]){
          pwm[i] = iValue;
          return;
        }
      }
    }

    static bool getValue(int iPin){
      uint8_t i = 0;
      for (i = 0; i < I2C_2313_IO_PINS_COUNT; i++){
        if (iPin == pinMap[i]){
          return bitRead(pinInputValues, i);
        }
      }
    }
};

I2c_management I2C_2313_Interface::connection;
uint8_t I2C_2313_Interface::pinValues = 0;
uint8_t I2C_2313_Interface::pinInputValues = 0;
uint8_t I2C_2313_Interface::pinInInputMode = 0;
uint8_t I2C_2313_Interface::pwmValues = 0;
uint8_t I2C_2313_Interface::pwm[3] = {0};
uint8_t I2C_2313_Interface::pinMap[] = {4,5,6,7,8,9,10,15};
uint8_t I2C_2313_Interface::pwmMap[] = {11,12,13};
#endif
