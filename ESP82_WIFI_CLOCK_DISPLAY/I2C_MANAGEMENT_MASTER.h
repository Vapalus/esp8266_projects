#ifndef I2C_MANAGEMENT_MASTER
#define I2C_MANAGEMENT_MASTER

class I2c_management{
  private:
    
  public:
    static void init(){//Maybe dangerous to call after the display setup?
      /*
      Wire.begin(sda, scl);
      */
    }

    static void getValues(uint8_t address, uint8_t* values, uint16_t expectedLength){
      Wire.requestFrom(address, expectedLength);
      int iCounter = 0;
      while (Wire.available() && iCounter < expectedLength) { // slave may send less than requested, or something funny happens
        values[iCounter++] = Wire.read(); // receive a byte as character
      }
    }

    static void setValues(uint8_t address, uint8_t* values, uint16_t valueArrayLength){
      Wire.beginTransmission(address);
      Wire.write(values,valueArrayLength);
      Wire.endTransmission();
    }

};
#endif
