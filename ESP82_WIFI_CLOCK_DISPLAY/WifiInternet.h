#ifndef WIFI_INTERNET
#define WIFI_INTERNET

#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include "InternetFunctions.h"
#include "KeyButtons.h"

#include <AceTime.h>
using namespace ace_time;
using namespace ace_time::clock;

//#define NTP_HOUR_OFFSET_WINTER 1
//#define NTP_HOUR_OFFSET_SUMMER 2

//Converted from:
//https://github.com/esp8266/Arduino/blob/master/libraries/ESP8266WiFi/examples/NTPClient/NTPClient.ino


//Holy shit this class is big. Too big. Need to split it into "NTP" and "Interface".
class WifiInternet{
  private:
    const static int NTP_PACKET_SIZE; // NTP time stamp is in the first 48 bytes of the message
    const static char * ssid; // your network SSID (name)
    const static char * pass;  // your network password
    static char * ntpServerName;
    static IPAddress timeServerIP;
    static WiFiUDP udp;
    static byte packetBuffer[];
    static char status;
    static char timeout;
    static uint8_t years;
    static uint8_t months;
    static uint8_t days;
    static uint8_t hours;
    static uint8_t minutes;
    static uint8_t seconds;

    static BasicZoneProcessor berlinProcessor;

    static void init_internal(){
      WiFi.disconnect();
      WiFi.mode(WIFI_AP);
      delay(10);
      WiFi.mode(WIFI_STA);
      WiFi.begin(ssid, pass);
      udp.begin(2390);
      status = 0;
      timeout = 100;
    }
    
    static void sendNTPpacket() {
      // set all bytes in the buffer to 0
      memset(packetBuffer, 0, NTP_PACKET_SIZE);
      // Initialize values needed to form NTP request
      // (see URL above for details on the packets)
      packetBuffer[0] = 0b11100011;   // LI, Version, Mode
      packetBuffer[1] = 0;     // Stratum, or type of clock
      packetBuffer[2] = 6;     // Polling Interval
      packetBuffer[3] = 0xEC;  // Peer Clock Precision
      // 8 bytes of zero for Root Delay & Root Dispersion
      packetBuffer[12]  = 49;
      packetBuffer[13]  = 0x4E;
      packetBuffer[14]  = 49;
      packetBuffer[15]  = 52;
    
      // all NTP fields have been given values, now
      // you can send a packet requesting a timestamp:
      udp.beginPacket(timeServerIP, 123); //NTP requests are to port 123
      udp.write(packetBuffer, NTP_PACKET_SIZE);
      udp.endPacket();
    }
    
  public:
    static uint8_t getHours(){
      return hours;
    }
    static uint8_t getMinutes(){
      return minutes;
    }
    static uint8_t getSeconds(){
      return seconds;
    }

    static void init(KeyButtons* keyboard){
      InternetFunctions::init(keyboard);
      init_internal();
      keyboard->activateKey(8);
      keyboard->activateKey(9);
      keyboard->activateKey(10);
      keyboard->activateKey(15);
    }

    static void deinit(){
      WiFi.disconnect();
      udp.stop();
      status = 0;
    }
    
    static bool isConnected(SSD1306Wire* display){
      if (WiFi.status() == WL_CONNECTED){
        display->drawString(0,0, "connected");
        status = 1;
        return true;
      }
      else{
        if (timeout--){
          display->drawString(0,0, "connecting...");
          status = 0;
        }
        else{
          display->drawString(0,0, "retry...");
          timeout = 100;
          init_internal();
        }
        return false;
      }
    }

    static void printIP(SSD1306Wire* display){
      WiFi.hostByName(ntpServerName, timeServerIP);
      char ip[16];
      ip[0] = timeServerIP[0]/100 + '0';
      ip[1] = (timeServerIP[0]/10)%10 + '0';
      ip[2] = timeServerIP[0]%10 + '0';
      ip[3] = ':';
      ip[4] = timeServerIP[1]/100 + '0';
      ip[5] = (timeServerIP[1]/10)%10 + '0';
      ip[6] = timeServerIP[1]%10 + '0';
      ip[7] = ':';
      ip[8] = timeServerIP[2]/100 + '0';
      ip[9] = (timeServerIP[2]/10)%10 + '0';
      ip[10] = timeServerIP[2]%10 + '0';
      ip[11] = ':';
      ip[12] = timeServerIP[3]/100 + '0';
      ip[13] = (timeServerIP[3]/10)%10 + '0';
      ip[14] = timeServerIP[3]%10 + '0';
      ip[15] = 0;
      display->drawString(0,20, ip);
    }

    static void printTime(SSD1306Wire* display){
      char timeString[9];
      timeString[0] = '0' + hours/10;
      timeString[1] = '0' + hours%10;
      timeString[2] = ':';
      timeString[3] = '0' + minutes/10;
      timeString[4] = '0' + minutes%10;
      timeString[5] = ':';
      timeString[6] = '0' + seconds/10;
      timeString[7] = '0' + seconds%10;
      timeString[8] = 0;
      display->drawString(0,20, timeString);
    }
    
    static void updateNTPIP(SSD1306Wire* display){
      WiFi.hostByName(ntpServerName, timeServerIP);
      sendNTPpacket();
      timeout = 5;
      status = 2;
    }
    
    static bool getTime(SSD1306Wire* display){
      if (timeout){
        display->drawString(0,0, "delay...");
        timeout--;
        return false;
      }
      int cb = udp.parsePacket();
      if (!cb) {
        display->drawString(0,0, "not received");
        printIP(display);
        status = 1;
        return false;
      } else {
        display->drawString(0,0, "time received");
        printIP(display);
        udp.read(packetBuffer, NTP_PACKET_SIZE); // read the packet into the buffer
        unsigned long highWord = word(packetBuffer[40], packetBuffer[41]);
        unsigned long lowWord = word(packetBuffer[42], packetBuffer[43]);
        unsigned long secsSince1900 = highWord << 16 | lowWord;
        const unsigned long seventyYears = 2208988800UL;
        // subtract seventy years:
        unsigned long epoch = secsSince1900 - seventyYears;

        auto berlinTz = TimeZone::forZoneInfo(&zonedb::kZoneEurope_Berlin, &berlinProcessor);
        auto berlinTime = ZonedDateTime::forEpochSeconds(epoch, berlinTz); //BERLIN TIME, as known in Poland
        //Summertime/Wintertime missing?
        //epoch += NTP_HOUR_OFFSET_WINTER * 3600;
        //epoch += NTP_HOUR_OFFSET_SUMMER * 3600;

        //hours = (epoch  % 86400L) / 3600;
        //minutes = (epoch  % 3600) / 60;
        //seconds = epoch % 60;
        years = berlinTime.year();
        months = berlinTime.month();
        days = berlinTime.day();
        hours = berlinTime.hour();
        minutes = berlinTime.minute();
        seconds = berlinTime.second();
      }
      udp.stop();
      return true;
    }

    static bool doLoop(SSD1306Wire* display, KeyButtons* keyboard){
      switch(status){
        case 0:
          isConnected(display);
          return false;
          break;
        case 1:
          updateNTPIP(display);
          return false;
          break;
        case 2:
          if (getTime(display)){
            status = 3;
            return true;
          }
          else{
            return false;
          }
          break;
        case 3:
          //Trying it with static function, just to figure out how it works
          if (WiFi.status() == WL_CONNECTED){
            if (InternetFunctions::doInternetStuff(display, keyboard)){
              display->drawString(0,52, "connected,time=synced");
            }
            else{
              //Needs to be implemented, since there may be a problem with the UDP connection which doesn't deinit correctly.
              WiFi.disconnect();
              WiFi.mode(WIFI_AP);
              delay(10);
              WiFi.mode(WIFI_STA);
              WiFi.begin(ssid, pass);
              display->drawString(0,52, "reconnecting");
            }
          }
          else{
            display->drawString(0,52, "disconnected. reconnecting");
          }
          return false;
          break;
        default:
          status = 0;
          return false;
          break;
      }
    }

    static void reconnect(){
      status = 0;
    }
};

const int WifiInternet::NTP_PACKET_SIZE = 48;
const char * WifiInternet::ssid = CONNECT_SSID;
const char * WifiInternet::pass = CONNECT_PASS;

uint8_t WifiInternet::years = 0;
uint8_t WifiInternet::months = 0;
uint8_t WifiInternet::days = 0;
uint8_t WifiInternet::hours = 0;
uint8_t WifiInternet::minutes = 0;
uint8_t WifiInternet::seconds = 0;

char WifiInternet::status = 0;
char WifiInternet::timeout = 0;

char * WifiInternet::ntpServerName = "time.nist.gov";
byte WifiInternet::packetBuffer[WifiInternet::NTP_PACKET_SIZE];
WiFiUDP WifiInternet::udp;
IPAddress WifiInternet::timeServerIP;

BasicZoneProcessor WifiInternet::berlinProcessor;
#endif
