#ifndef TIME_H
#define TIME_H

class ShowTheTime {
  private:
    unsigned long timeNow = 0;
    unsigned long offset = 0;
    unsigned long overflowProtection = 0;
    long dailyError = 0; 

    void updateTime(){
      unsigned long currentMillis = millis();
      static unsigned long lastMillis = 0;
      static unsigned long lastOffset = offset;
      timeNow = currentMillis + offset;// + (dailyError/86400000L);}//remove the ";//" if you want to counteract oscillator errors (which is bogus imho)
      //Counteract "millis()" overflow
      //Unsigned Long means 4,294,967,295 for arduino. It might be something else for other systems!.
      //So for arduino, the difference between max and zero mod 86400000L is 61367295L.
      if (lastOffset != offset){
        lastOffset = offset;
      }
      else{
        if (timeNow < currentMillis){ //This is for the time during which the offset overflows the timeNow variable
          timeNow += 61367296L;//At 0 it must be the same as at 4,294,967,295, plus 1
        }
        if (lastMillis > currentMillis){ //This is for the moment the millis() reset to zero
            offset += 61367296L;//At 0 it must be the same as at 4,294,967,295, plus 1
            offset %= 86400000L;
        }
      }
    }
    
    byte getHour(){return ((timeNow/1000)/3600)%24;}

    byte getMinute(){return ((timeNow/1000)/60)%60;}

    byte getSecond(){return (timeNow/1000)%60;}

    unsigned long calculateOffset(uint8_t hour, uint8_t minute, uint8_t second){
        return ((unsigned long)hour*1000*3600) + ((unsigned long)minute*1000*60) + ((unsigned long)second*1000); 
    }

  public:
    ShowTheTime(uint8_t hour, uint8_t minute, uint8_t second){
      offset = calculateOffset(hour, minute, second);
    }

    void setTime(uint8_t hour, uint8_t minute, uint8_t second){
        unsigned long expectedShownTime = calculateOffset(hour, minute, second) + 86400000UL;
        unsigned long currentShownTime = millis()%86400000L;
        offset = (expectedShownTime - currentShownTime)%86400000UL;
        //offset = (expectedShownTime - currentShownTime);//%86400000UL;
    }
    
    void runTime(char *timeString){
      updateTime();
      byte hours = getHour();
      byte minutes = getMinute();
      byte seconds = getSecond();

      //hh:mm:ss
      timeString[0] = '0' + hours/10;
      timeString[1] = '0' + hours%10;
      timeString[2] = ':';
      timeString[3] = '0' + minutes/10;
      timeString[4] = '0' + minutes%10;
      timeString[5] = ':';
      timeString[6] = '0' + seconds/10;
      timeString[7] = '0' + seconds%10;
      timeString[8] = 0;
    }
};
#endif
