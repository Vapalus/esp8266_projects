#ifndef SCANPROBE_H
#define SCANPROBE_H

#include <Arduino.h>
#include <Wire.h>  

#define PROBESCAN_MAX_COUNT 5
#define PROBESCAN_SSID_LEN 24
#define PROBESCAN_CHANNEL_LEN 4

#define PROBESCAN_DATA_LENGTH           112

#define PROBESCAN_TYPE_MANAGEMENT       0x00
#define PROBESCAN_TYPE_CONTROL          0x01
#define PROBESCAN_TYPE_DATA             0x02
#define PROBESCAN_SUBTYPE_PROBE_REQUEST 0x04

#define PROBESCAN_CHANNEL_HOP_INTERVAL_MS   1000

#define DISABLE 0
#define ENABLE  1

//Converted from:
//https://stackoverflow.com/questions/45089986/esp8266-sniffing-and-sending-mac-address

extern "C" {
  #include <user_interface.h> //if that doesn't work, you probably forgot to put the right system
}

class ScanForProbe {
  private:
  	static int CHANs[PROBESCAN_MAX_COUNT];// = {0};
  	static char SSIDs[PROBESCAN_MAX_COUNT][PROBESCAN_SSID_LEN];// = {{0}};
    static os_timer_t channelHop_timer;
  
  	struct RxControl {
  	 signed rssi:8; // signal intensity of packet
  	 unsigned rate:4;
  	 unsigned is_group:1;
  	 unsigned:1;
  	 unsigned sig_mode:2; // 0:is 11n packet; 1:is not 11n packet;
  	 unsigned legacy_length:12; // if not 11n packet, shows length of packet.
  	 unsigned damatch0:1;
  	 unsigned damatch1:1;
  	 unsigned bssidmatch0:1;
  	 unsigned bssidmatch1:1;
  	 unsigned MCS:7; // if is 11n packet, shows the modulation and code used (range from 0 to 76)
  	 unsigned CWB:1; // if is 11n packet, shows if is HT40 packet or not
  	 unsigned HT_length:16;// if is 11n packet, shows length of packet.
  	 unsigned Smoothing:1;
  	 unsigned Not_Sounding:1;
  	 unsigned:1;
  	 unsigned Aggregation:1;
  	 unsigned STBC:2;
  	 unsigned FEC_CODING:1; // if is 11n packet, shows if is LDPC packet or not.
  	 unsigned SGI:1;
  	 unsigned rxend_state:8;
  	 unsigned ampdu_cnt:8;
  	 unsigned channel:4; //which channel this packet in.
  	 unsigned:12;
  	};
  
  	struct SnifferPacket{
  		struct RxControl rx_ctrl;
  		uint8_t data[PROBESCAN_DATA_LENGTH];
  		uint16_t cnt;
  		uint16_t len;
  	};
  
  	// Declare each custom function (excluding built-in, such as setup and loop) before it will be called.
  	// https://docs.platformio.org/en/latest/faq.html#convert-arduino-file-to-c-manually
  	static void showMetadata(SnifferPacket *snifferPacket);
  	static void ICACHE_FLASH_ATTR sniffer_callback(uint8_t *buffer, uint16_t length);
  	static void printDataSpan(uint16_t start, uint16_t size, uint8_t* data);
  	static void getMAC(char *addr, uint8_t* data, uint16_t offset);
    static void addChannel(char* strSSID, byte iChannel);
  	static void channelHop();
  public:
    void init();
    void deinit();
    void getScan(SSD1306Wire* display);
};

int ScanForProbe::CHANs[PROBESCAN_MAX_COUNT] = {0};
char ScanForProbe::SSIDs[PROBESCAN_MAX_COUNT][PROBESCAN_SSID_LEN] = {{0}};
os_timer_t ScanForProbe::channelHop_timer;
    
void ScanForProbe::addChannel(char* strSSID, byte iChannel){
  int iLength = 0;
  while(strSSID[iLength++]);
  if (iLength >= PROBESCAN_SSID_LEN){
    iLength = PROBESCAN_SSID_LEN - 1;
    strSSID[iLength] = 0;
  }
  int i = 0;
  int j = 0;
  int iStart = 0;
  int iStepSize = 12;
  for(i = 0; i < PROBESCAN_MAX_COUNT; i++){
  	if (strcmp(strSSID, SSIDs[i]) == 0){// && iChannel == CHANs[i]){
      CHANs[i] = iChannel;
	    return;
	  }
  }
  static int iPos = 0;
  i = iPos;
  for (j = 0; j < iLength; j++){
	  SSIDs[iPos][j] = strSSID[j];
  }
  SSIDs[iPos][j] = 0;
  CHANs[iPos] = iChannel;
  iPos++;
  if (iPos >= PROBESCAN_MAX_COUNT){
  	iPos = 0;
  }
}
  
void ScanForProbe::showMetadata(SnifferPacket *snifferPacket) {
  
  unsigned int frameControl = ((unsigned int)snifferPacket->data[1] << 8) + snifferPacket->data[0];
  
  uint8_t version      = (frameControl & 0b0000000000000011) >> 0;
  uint8_t frameType    = (frameControl & 0b0000000000001100) >> 2;
  uint8_t frameSubType = (frameControl & 0b0000000011110000) >> 4;
  uint8_t toDS         = (frameControl & 0b0000000100000000) >> 8;
  uint8_t fromDS       = (frameControl & 0b0000001000000000) >> 9;
  
  // Only look for probe request packets
  if (frameType != PROBESCAN_TYPE_MANAGEMENT || frameSubType != PROBESCAN_SUBTYPE_PROBE_REQUEST){
		return;
  }
 
  char addr[] = "00:00:00:00:00:00";
  getMAC(addr, snifferPacket->data, 10);
  	  
  uint8_t PROBESCAN_SSID_LENgth = snifferPacket->data[25];
  	  
  //printDataSpan(26, PROBESCAN_SSID_LENgth, snifferPacket->data);
  char strSSID[PROBESCAN_SSID_LEN] = {0};
 	  
  for(uint16_t i = 26; i < PROBESCAN_DATA_LENGTH && i < 26+PROBESCAN_SSID_LENgth && i < 26+PROBESCAN_SSID_LEN; i++) {
 		strSSID[i-26] = snifferPacket->data[i];
  }
  strSSID[PROBESCAN_SSID_LEN-1] = 0;
  if(strSSID[0]){
 		addChannel(strSSID,wifi_get_channel());
  }
}
  
  	/**
  	 * Callback for promiscuous mode
  	 */
void ICACHE_FLASH_ATTR ScanForProbe::sniffer_callback(uint8_t *buffer, uint16_t length) {
  struct SnifferPacket *snifferPacket = (struct SnifferPacket*) buffer;
  showMetadata(snifferPacket);
}

void ScanForProbe::getMAC(char *addr, uint8_t* data, uint16_t offset) {
  sprintf(addr, "%02x:%02x:%02x:%02x:%02x:%02x", data[offset+0], data[offset+1], data[offset+2], data[offset+3], data[offset+4], data[offset+5]);
}
  
  	/**
  	 * Callback for channel hoping
  	 */
void ScanForProbe::channelHop()
{
  // hoping channels 1-13
  uint8 new_channel = wifi_get_channel() + 1;
  if (new_channel > 13) {
	  new_channel = 1;
  }
  wifi_set_channel(new_channel);
}
  	
void ScanForProbe::init() {
  wifi_set_opmode(STATION_MODE);
  wifi_set_channel(1);
  wifi_promiscuous_enable(DISABLE);
  delay(10);
  wifi_set_promiscuous_rx_cb(ScanForProbe::sniffer_callback);
  delay(10);
  wifi_promiscuous_enable(ENABLE);

  // setup the channel hoping callback timer
  os_timer_disarm(&ScanForProbe::channelHop_timer);
  os_timer_setfn(&ScanForProbe::channelHop_timer, (os_timer_func_t *) ScanForProbe::channelHop, NULL);
  os_timer_arm(&ScanForProbe::channelHop_timer, PROBESCAN_CHANNEL_HOP_INTERVAL_MS, 1);
}

void ScanForProbe::deinit(){
  os_timer_disarm(&ScanForProbe::channelHop_timer);
  
  wifi_set_opmode(STATION_MODE);
  wifi_promiscuous_enable(DISABLE);
  
  delay(10);
}
  	
void ScanForProbe::getScan(SSD1306Wire* display){
  int i = 0;
  int iStart = 0;
  int iStepSize = 12;
  for(i = 0; i < PROBESCAN_MAX_COUNT; i++){
 		if (!SSIDs[i][0]){
      display->drawString(0,iStart + iStepSize*i, "-");
 		}
 		SSIDs[i][PROBESCAN_SSID_LEN-1] = 0;
 		display->drawString(0,iStart + iStepSize*i, SSIDs[i]);
 		char cstr[PROBESCAN_CHANNEL_LEN];
 		itoa(CHANs[i],cstr,10);
 		cstr[PROBESCAN_CHANNEL_LEN-1] = 0;
 		if (CHANs[i] < 10){
 		  display->drawString(122,iStart + iStepSize*i, cstr);
 		}
 		else{
 		  display->drawString(116,iStart + iStepSize*i, cstr);
 		}
  }
  
}

#endif
