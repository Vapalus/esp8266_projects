#ifndef KEY_BUTTONS
#define KEY_BUTTONS
#include "I2C_2313_Interface.h"

class KeyButtons{
  private:
    uint8_t* keysPressed;
    uint8_t keyCount;
    I2C_2313_Interface interface;
  public:
    KeyButtons(){
      
    }
  
    void init(uint8_t keyCount){
      interface.init();
      keysPressed = new uint8_t[keyCount];
      for (int i = 0; i < keyCount; i++){
        keysPressed[i] = false;
      }
    }

    void activateKey(uint8_t key){
        interface.setMode(key,1); //input
        interface.update_2313();
    }

    void update(){
      interface.update_self();
    }
    
    bool wasReleased(uint8_t key){
      //interface.update_self();
      if (interface.getValue(key)){
        if (!(keysPressed[key] & 2)){
          keysPressed[key] |= 2;
          return true;
        }
      }
      else{
        keysPressed[key] &= ~2;
      }
      return false;
    }

    bool wasPressed(uint8_t key){
      if (!(interface.getValue(key))){
        if ((keysPressed[key] & 1)){
          keysPressed[key] &= ~1;
          return true;
        }
      }
      else{
        keysPressed[key] |= 1;
      }
      return false;
    }
    
    void resetKey(uint8_t key){
      interface.update_self();
      uint8_t value = interface.getValue(key);
      keysPressed[key] = value;
    }
};
#endif
