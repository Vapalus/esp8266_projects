#define CONNECT_SSID "SSID"
#define CONNECT_PASS "PASS"
#include "WiFiPassword.h" //Does not exist for YOUR release (because of reasons, guess why), so just remove this line.

#include <Wire.h>  
#include "SSD1306Wire.h"
#include "ShowTheTime.h"
#include "ScanForProbe.h"
#include "KeyButtons.h"
#include "WifiScanner.h"
#include "WifiInternet.h"
#include "LearnChinese.h"
#include "Tetris.h"
#define SDA 0
#define SCL 2

SSD1306Wire display(0x3c, SDA, SCL);


ShowTheTime actual_watch(13,54,00);
ScanForProbe probe_scanner;
KeyButtons keyboard;
WifiScanner wifi_scanner;
WifiInternet internet;
LearnChinese chinese;
TetrisField tetris;

void init_probescanner(){
  probe_scanner.init();
}

void deinit_probescanner(){
  probe_scanner.deinit();
}

void init_wardriver(){
  wifi_scanner.init(&keyboard);
}

void deinit_wardriver(){
  wifi_scanner.deinit();
}

void init_internet(){
  internet.init(&keyboard);
}

void deinit_internet(){
  internet.deinit();
}

void init_display(){
  delay(100);
  display.init(); //This command is necessairy for the OLED display to work. Initializing OTHER I2C objects may result in problems.
  display.flipScreenVertically();
  display.setBrightness(255);
}

void init_keyboard(){
  delay(100);
  keyboard.init(16);
  keyboard.activateKey(4);
  keyboard.activateKey(5);
}

void init_chinese(){
  display.setFont(ArialMT_Plain_10);
  chinese.init(&keyboard);
}

void init_tetris(){
  tetris.init();
}
///////////////////////////////////////////////////////////

void doTime(){
  static bool blnShowTime = true;
  if (keyboard.wasPressed(5)){
    blnShowTime = !blnShowTime;
  }
  if (blnShowTime){
    char timeString[9];
    actual_watch.runTime(timeString);
    display.setFont(ArialMT_Plain_24);
    display.drawString(10,30, timeString);
  }
  else{
    //display.clear();
  }
}

void doProbeScan(){
  display.setFont(ArialMT_Plain_10);//10, 16, 24
  probe_scanner.getScan(&display);
}

void doWarDrive(){
  display.setFont(ArialMT_Plain_10);//10, 16, 24
  wifi_scanner.getScan(&display, &keyboard);
}

void doInternet(){
  display.setFont(ArialMT_Plain_10);//10, 16, 24
  if (internet.doLoop(&display, &keyboard)){
    actual_watch.setTime(internet.getHours(), internet.getMinutes(), internet.getSeconds());
  }
}

void doChineseLearning(){
  chinese.draw(&display, &keyboard);
}

void playTetris(){
  tetris.run(&display, &keyboard);
}

///////////////////////////////////////////////////////////

void setup(){
  delay(300); //ATTACK! time
  init_display();
  init_keyboard();
}

void loop(){
  static int displayMode = 0;
  keyboard.update();
  display.clear();
  displayMode += keyboard.wasPressed(4);
  
  switch(displayMode){
    case 0:
      doTime();
      break;
    case 1:
      init_probescanner();
      displayMode++;
      break;
    case 2:
      doProbeScan();
      break;
    case 3:
      deinit_probescanner();
      displayMode++;
      break;
    case 4:
      init_wardriver();
      displayMode++;
      break;
    case 5:
      doWarDrive();
      break;
    case 6:
      deinit_wardriver();
      displayMode++;
      break;
    case 7:
      init_internet();
      displayMode++;
      break;
    case 8:
      doInternet();
      break;
    case 9:
      deinit_internet();
      init_chinese();
      displayMode++;
      break;
    case 10:
      doChineseLearning();
      break;
    case 11:
      init_tetris();
      displayMode++;
      break;
    case 12:
      playTetris();
      break;
    default:
      doTime();
      displayMode = 0;
      break;
  }

  display.display();
  delay(100);
}
