#ifndef FUNC_INTERFACE
#define FUNC_INTERFACE


class FunctionInterface {
  public:             
    void init();
    void oncePerMinute();
    void deinit();
};

#endif
