/*
Author:
Pranav Cherukupalli <cherukupallip@gmail.com>
*/

#include "BasicESP32Functions.h"
#include "GeneralSmartwatchFunctions.h"



void setup(){
  int iWakeupReason = getWakeupReason();
  init_display();

  if (iWakeupReason < 0){
    get_time();
  }
  draw_time();
  show_display();
  
  start_sleeping();
}

void loop(){
  //If currently not "just showing time" this is IT!
}
