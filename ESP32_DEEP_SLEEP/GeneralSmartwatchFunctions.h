#ifndef GENERAL
#define GENERAL

#define WAIT_FOR_SERIAL 1000 
#define NTP_SERVER "de.pool.ntp.org"
//#define TZ_INFO "WEST-1DWEST-2,M3.5.0/02:00:00,M10.5.0/03:00:00" // Western European Time
#define TZ_INFO ""

#include <Wire.h>
#include "SSD1306.h"
#include <WiFi.h>
#include "BasicESP32Functions.h"
#include "Passwords.h"

//RTC_DATA_ATTR int bootCount;
SSD1306 display(0x3c, 21, 22);
int hoursX[] = {0,3,7,10,13,16,19,21,24,26,28,29,30,31,32,32,32,31,30,29,28,26,24,21,19,16,13,10,7,3,-0,-3,-7,-10,-13,-16,-19,-21,-24,-26,-28,-29,-30,-31,-32,-32,-32,-31,-30,-29,-28,-26,-24,-21,-19,-16,-13,-10,-7,-3};
int hoursY[] = {-32,-32,-31,-30,-29,-28,-26,-24,-21,-19,-16,-13,-10,-7,-3,0,3,7,10,13,16,19,21,24,26,28,29,30,31,32,32,32,31,30,29,28,26,24,21,19,16,13,10,7,3,-0,-3,-7,-10,-13,-16,-19,-21,-24,-26,-28,-29,-30,-31,-32};
void get_time()
{
  WiFi.mode(WIFI_STA);
  WiFi.begin(WIFI_NAME, WIFI_PASS);
  
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(50);
  }
  
  struct tm local;
  configTzTime(TZ_INFO, NTP_SERVER); // ESP32 Systemzeit mit NTP Synchronisieren
  getLocalTime(&local, 10000);      // Versuche 10 s zu Synchronisieren
  WiFi.mode(WIFI_OFF);
  display.drawString(0, 0, "synced");
}

void init_display(){
  display.init();
}

void draw_time(){
  time_t now;
  char strftime_buf[64];
  struct tm timeinfo;
  
  time(&now);
  setenv("TZ", "UTC-1", 1);
  localtime_r(&now, &timeinfo);
  strftime(strftime_buf, sizeof(strftime_buf), "%T", &timeinfo);

  int h = timeinfo.tm_hour;
  int m = timeinfo.tm_min;
  //display.drawString(0, 0, strftime_buf);
  //128 x 64
  display.drawCircle(64, 32, 2);

  
  h %= 12;
  //m += 2;
  //m /= 5;
  m %= 60;

  display.fillRect(64+hoursX[h*5]/2, 32+hoursY[h*5]/2, 2,2);
  display.fillRect(64+(hoursX[m]*2)/3, 32+(hoursY[m]*2)/3, 1,1);
}

void show_display(){
  display.display();
}

#endif
