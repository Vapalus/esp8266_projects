#ifndef ESP32_BASIC_FUNCTIONS
#define ESP32_BASIC_FUNCTIONS


#define uS_TO_S_FACTOR 1000000  
#define TIME_TO_SLEEP  60        


void start_sleeping(){
  esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * uS_TO_S_FACTOR);
  esp_sleep_enable_ext0_wakeup((gpio_num_t)0,LOW);
  
  /*
  http://esp-idf.readthedocs.io/en/latest/api-reference/system/deep_sleep.html
  */
  //esp_deep_sleep_pd_config(ESP_PD_DOMAIN_RTC_PERIPH, ESP_PD_OPTION_OFF);

  esp_deep_sleep_start();
}

int getWakeupReason(){
  esp_sleep_wakeup_cause_t wakeup_reason = esp_sleep_get_wakeup_cause();

  switch(wakeup_reason)
  {
    case ESP_SLEEP_WAKEUP_TIMER : 
      return 0;
    case ESP_SLEEP_WAKEUP_EXT0 : 
      return 1;
    case ESP_SLEEP_WAKEUP_EXT1 : 
      return 2;
    case ESP_SLEEP_WAKEUP_TOUCHPAD : 
      return 3;
    case ESP_SLEEP_WAKEUP_ULP : 
      return 4;
    default : 
      return -1;
  }
}

#endif
